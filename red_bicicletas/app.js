require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');

let store;
if(process.env.NODE_ENV === 'development' ) {
  //we save the session in the server (Note. If the server trun off the session is lost)
   store = new session.MemoryStore
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  })
}

var app = express();
app.set('secretKey', 'jwt_pwd_!!123465');
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_!!'
}));

var mongoose = require('mongoose');
const { token } = require('morgan');
const usuario = require('./models/usuarios');

//var mongoDB = 'mongodb://localhost/red_bicicletas';
//mongodb+srv://admin:<password>@red-bicicletas.shedf.mongodb.net/<dbname>?retryWrites=true&w=majority
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {useNewUrlParser: true,useUnifiedTopology: true} );
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB conecction error '));



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/login', function(req,res){
  res.render('session/login');
});

app.post('/login', function(req,res, next){
  passport.authenticate('local', function(err, user, info){
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req,res, next);
});

app.get('/logout', function(req,res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', (req, res) => {
  res.render('usuarios/forgotPassword', { message: { desc: '*Se enviara un link de validacion a la direccion de correo' } });
});

app.post('/forgotPassword', (req, res, next) => {
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
      if (!usuario) return res.render('usuarios/forgotPassword', { message: { error: '*El email ingresado no está asociado a una cuenta' } });
      usuario.verificado = false;
      usuario.enviar_mail_resetPassword();
      usuario.save(function(err) {
          if (err) return res.status(400).send({ type: 'not-verified', msg: 'Error inesperado' });
      });

      res.render('usuarios/forgotPassword', { message: { mail_ok: true, desc: '*Se envio el email. Verificar su casilla de correo' } });

  });


});

app.get('/token/resetPassword/:token', (req, res, next) => {
  Token.findOne({ token: req.params.token }, function(err, token) {
      if (!token) return res.status(400).send({ type: 'not-verified', msg: 'Link de verificacion invalido. Reintentar' });
      Usuario.findById(token._userId, function(err, usuario) {
          if (!usuario) return res.status(400).send({ type: 'not-verified', msg: 'No se encontro el usuario asociado' });

          res.render('usuarios/resetPassword', { message: { errors: {}, usuario } });
      })

  });
});

app.post('/resetPassword', (req, res) => {
  if (req.body.password != req.body.confirm_password) {
      res.render('usuarios/resetPassword', { message: { error: 'No coinciden los valores ingresados', usuario: { email: req.body.email } } })
      return;
  }

  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
      usuario.password = req.body.password;
      usuario.verificado = true;
      usuario.save(function(err) {
          if (err) return res.status(400).send({ type: 'not-verified', msg: 'Error inesperado' });
          res.render('session/login', { errors: {} });
      });
  })
});

//Login google
app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read']}));

app.get('/auth/google/callback',passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
})
);


app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/bicicletas', loggedIn, bicicletasRouter);

app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario , bicicletasAPIRouter);
app.use('/api/usuarios',usuariosAPIRouter);

app.use('/privacy_policy', function(req,res){
  res.sendFile('public/policy_privacy.html');
})

app.use('/google284e265c6fd3a5af', function(req,res){
  res.sendFile('public/google284e265c6fd3a5af.html');
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
      next();
  } else {
      console.log('user sin loguarse');
      res.render('session/login', { errors: {} });
  }
};

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decode) {
      if (err) {
          res.json({ status: "error", message: err.message, data: null });
      } else {
          // add user id to request
          //console.log('jwt verified', decode)
          req.body.userId = decode.id;
          next();
      }
  });
};

module.exports = app;
