const moongose = require('mongoose');
const { model } = require('./bicicleta');

const Schema = moongose.Schema;
const TokenSchema = new Schema({
    _userId: { type: moongose.Schema.Types.ObjectId, required: true, ref: 'Usuario'},
    token: { type: String, required: true},
    createdAt: { type: Date, required: true, default: Date.now, expires: 43200}
});

module.exports = moongose.model('Token', TokenSchema);